# GitLab

GitLab is one of many Git remote services. GitHub is an alternative and is
currently the most popular.

GitLab and its contemporaries are widely used by open source projects for
collaboration purposes. However, GitLab (along with other remote services)
has more collaboration tools than just a Git repository. Among them are:

- Issues - A method for following the detection of bugs, request of
features, completion of tasks, or anything that requires a change of code.
- Merge Requests - Used to ask for changes in a branch or fork to be merged to
another branch or fork. In addition here, is the process of code review (more
on that later).
- CI/DC - Continuous Integration/Continuous Deployment. These
are most commonly used for automatically testing code and deploying it.

## Issues

An issue represents one of many things, but in the end represents something which
would need a change of code to resolve.

When creating a new issue, provide a succinct, useful title so that other
developers know what the bug or request is about. In addition, in the description
provide as much detail as possible about what needs to be changed. Some pieces
which are usually useful are:

- A brief description of either what the bug or feature is/what is being asked for
- For a bug, what is happening versus what should be happening
- Any relevant code snippets (You can use two sets of three back-ticks, `, to
create a code block which makes reading this much easier)

Issues in GitLab are assigned a number and represented throughout as
`#<issue number>`. This can be used to reference them in other things
such as commit messages, other issues, and merge requests.

![GitLab Issue](resources/gitlab_issue.png)

### Assignees

Assignees are used in issues to denote who is responsible for that issue. This
typically manifests in who is responsible for implementing the feature or fixing
the bug, however it can be used for other purposes as well.

### Labels

Issues can be given labels to help group them together or mark them as a
particular type. A single issue can be assigned any number of labels.

### Milestones

Milestones group issues together for tracking which issues are part of larger
initiatives, such as releases. ***Note:*** These are distinct from Projects.

## Boards

Boards allow for the grouping of issues, similar to milestones, but in a manner
which allows for a quick glance of the progress of each issue within the repository.

## Merge Requests (MR)

A Merge Request is created as a way to ask for changes from a branch or fork to be
merged into a branch (usually the main branch) in this repository. Similar to issues,
pull requests are given a number which can be used to reference them, however, they
are prefixed with a `!` rather than `#` (ex. `!<merge request number>`).

Creating a merge request should be done in a similar manner to creating an issue,
by providing a brief title that describes that changes enclosed in the merge
request as well as a detailed description of what the changes are intended to
accomplish. However, in addition here, any issues related to this merge request
should be mentioned in the description.

***Note:*** You can mention an issue with terms like `closes`, `fixes`, or
`resolves` just before the issue reference to automatically close the issue
when the merge request is merged.

![GitLab Merge Request](resources/gitlab_merge_request.png)

### **Code Review**

Code review is the process of double checking code being merged into the
main branch for errors, confusing code, missing pieces, or any number of things.
This process is mainly intended to catch problems before they become part of
the main code base. Though, a beneficial side effect of this is also that more
people become familiar with the changes.

### Reviewers

Individuals who should perform a code review of the changes before they are
merged.

### MR Assignees

Similar to issues, the individuals who are responsible for the merge request. This
can represent developers who are responsible for merging the merge request or
otherwise need to view the merge request. There are also many other ways this
can be used, however, be sure that anyone who needs to review the code changes
is a `reviewer` rather than just an `assignee`.

### MR Labels

See `Labels` in the `Issues` section.

### MR Milestones

See `Milestones` in the `Issues` section.
