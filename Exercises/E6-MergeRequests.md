# Merge Requests

1. Create a Merge Request for one of the branches you've created in a previous
exercise.
2. Assign the Merge Request to yourself
3. Leave a comment on one of the files in the Merge Request
4. Add the `Bug` label to the Merge Request
5. Merge the Merge Request

---------

## ***Hints***

<!-- markdownlint-disable MD033 -->
<details>
    <summary>Hint 1</summary>
    <blockquote>
Don't forget to push your branch before creating a Merge Request.
    </blockquote>
</details>
<!-- markdownlint-enable MD033 -->
